package adventchallenge;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Day2 {

	public static void main(String[] args) throws IOException{
//		Day2 text = new Day2();
		// create list from reading in a file contents
		List<String> lines = readSmallTextFile("/home/dogma/workspace/JAVA_algajatele/inputs/inputday2");
		int length, width, height;
		int smallest;
		int paper;
		int sum = 0;
		int smallesRibbon = 0;
		int ribbonForBow = 0;
		int totalRibbonNeeded = 0;
		
		for (String string : lines) {
			length = Integer.parseInt(string.substring(0, string.indexOf("x")));
			width = Integer.parseInt(string.substring(string.indexOf("x") + 1, string.lastIndexOf("x")));
			height = Integer.parseInt(string.substring(string.lastIndexOf("x") + 1));
			
			smallest = 	Math.min(Math.min(length * width, length * height), width * height);
			
			smallesRibbon =  Math.min(Math.min((2 * length) + (2 * width), (2 * length) + (2 * height)), (2 * width) + (2 * height));
			ribbonForBow = length * width * height;
			totalRibbonNeeded = smallesRibbon + ribbonForBow;
					
			paper = (2 * length * width) + (2 * width * height) + (2 * height * length) + smallest;
			sum += totalRibbonNeeded;
			
		}
		System.out.println(sum);
		
		
	} 
	
	final static Charset ENCODING = StandardCharsets.UTF_8;
	//method for reading all lines from a file
	static List<String> readSmallTextFile(String aFileName) throws IOException {
		Path path = Paths.get(aFileName);
		return Files.readAllLines(path, ENCODING);
	}

}
