package adventchallenge;

import java.awt.Point;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Day3 {

	public static void main(String[] args) throws IOException {
		//read file contents into list
		List<String> input = Day2.readSmallTextFile("/home/dogma/workspace/JAVA_algajatele/inputs/inputday3");
		//make a list to string
		String dir = input.toString();
		//make a string to char array
		char[] directions = dir.toCharArray();
		//variable for counting how many houses the santa has visited
		boolean santasTurn = true;
		Point house = new Point(0, 0);
		Point santaHouse = house;
		Point robotHouse = house;
	
		int houseVisited = 0;
		//create arraylist of point objects
		List<Point> map = new ArrayList<Point>();


		for (int i = 0; i < directions.length; i++) {
			Point p = santasTurn ? santaHouse : robotHouse;
			switch (directions[i]) {
			case ('^'):
				p = new Point(p.x, p.y + 1);
				break;
			case ('>'):
				p = new Point(p.x + 1, p.y);
				break;
			case ('v'):
				p = new Point(p.x, p.y -1);
				break;
			case ('<'):
				p = new Point(p.x -1, p.y);
				break;
			default: continue;
			}
			
			if(map.contains(p) == false){
				houseVisited++;
				map.add(p);
			}
			if(santasTurn){
				santaHouse = p;
			}
			else {
				robotHouse = p;
			}

			if(true){
				santasTurn = !santasTurn;
				System.out.println(santasTurn);
			}
			
		}
		System.out.println("Houses visited: " + houseVisited);
	}
}
