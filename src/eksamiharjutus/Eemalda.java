package eksamiharjutus;

import java.util.ArrayList;

public class Eemalda {
	public static void main(String[] args) {
		String [] numbrid = {"üks", "kaks", "kolm", "neli", "viis"};
		for (String string : eemaldaEssiga(numbrid)) {
			System.out.println(string);
		}
	}
	
	public static String[] eemaldaEssiga(String[] sonad){
		ArrayList<String> list = new ArrayList<String>();
		for (String string : sonad) {
			if(!string.contains("s")){
				list.add(string);
			}
		}
		String[] essita = new String[list.size()];
		list.toArray(essita);
		return essita;
	}
}
