package eksamiharjutus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class KasvavJada {

	public static void main(String[] args) {
		int[] suvalinejada = { -3, 1, 2, 1, 5, 3, 4, 7, 5 };

		System.out.println(Arrays.toString(eemaldaKahanemised(suvalinejada)));
	}

	public static int[] eemaldaKahanemised(int[] arvud) {
		int max = arvud[0];
		List<Integer> list = new ArrayList<Integer>();
		list.add(max);
		for (int i = 0; i < arvud.length; i++) {
			if (arvud[i] > max) {
				max = arvud[i];
				list.add(max);
			}
		}
		int[] kasvav = new int[list.size()];
		for (int i = 0; i < kasvav.length; i++) {
			kasvav[i] = list.get(i);
		}

		return kasvav;
	}

}
