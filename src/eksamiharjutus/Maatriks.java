package eksamiharjutus;

import java.util.Arrays;

public class Maatriks {
	public static void main(String[] args) {
		
		int[][] suvalinemaatriks = {
				{0, 3, 5, 6},
				{0, 0, 5, 0},
				{0, 0, 2, 1}
		};
		for (int[] is : taidaNullid(suvalinemaatriks)) {
			System.out.println(Arrays.toString(is));
		}
		
	}
	
	public static int[][] taidaNullid(int[][] maatriks){
		int[][] uus = new int[maatriks.length][maatriks[0].length];
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks[i].length; j++) {
				if(i > 0 && maatriks[i][j] == 0){// esimest rida ei puutu
					uus[i][j] = maatriks[i-1][j] + 1;
					maatriks[i][j] = uus[i][j];	//kirjutame algse maatriksi positsioonil i,j yle
				}
				else {
					uus[i][j] = maatriks[i][j];
				}
			}
		}
		
		return uus;
	}
}
