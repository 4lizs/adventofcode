package eksamiharjutus;

public class Tagurpidi {
	// "kas aias sadas saia?" -> "saia? sadas aias kas"
	public static String tagurpidiSonad(String tekst){
		
		String[] words = tekst.split(" ");//teeme s]nade massiivi
		String backwards = "";//Siia hakkame lisama s6nasid
		for (int i = words.length - 1; i >= 0; i--) {
			backwards += words[i] + " ";
		}
		
		return backwards;
	}
	public static void main(String[] args) {
		String lause = "kas aias sadas saia?";
		System.out.println(tagurpidiSonad(lause));
	}

}
