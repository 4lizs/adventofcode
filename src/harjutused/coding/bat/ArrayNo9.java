package harjutused.coding.bat;

import java.util.ArrayList;
import java.util.Arrays;

//Return an array that is "left shifted" by one -- so {6, 2, 5, 3} returns {2, 5, 3, 6}. You may modify and return the given array, or return a new array. 
//
//shiftLeft({6, 2, 5, 3}) → {2, 5, 3, 6}
//shiftLeft({1, 2}) → {2, 1}
//shiftLeft({1}) → {1}
public class ArrayNo9 {
	
	public static int[] shiftLeft(int[] nums){
		
		int[]shifted = new int[nums.length]; // this int array holds the shifted array
		
		for (int i = 0; i < nums.length; i++) {
			if(i + 1 < nums.length){
				shifted[i] = nums[i + 1];
			}
			else
				shifted[i] = nums[0];

		}
		return shifted;
		
	}
	
	public static void main(String[] args) {
		int[] test = {6, 2, 5, 3};
		System.out.println(Arrays.toString(shiftLeft(test)));
	}
}
